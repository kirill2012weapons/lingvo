<?php

namespace LingvoBundle\Repository;


use Doctrine\ORM\EntityRepository;
use LingvoBundle\Entity\Translating\CardInformation;
use LingvoBundle\Entity\Translating\English;
use LingvoBundle\Entity\Translating\Interfaces\IGetTranslates;
use LingvoBundle\Entity\Translating\Russian;

class EnglishRepository extends EntityRepository implements IGetTranslates
{

    public function getTranslatesAsArray()
    {
        return $this->createQueryBuilder('english')
            ->select('english')
            ->innerJoin(Russian::class, 'russian')
            ->getQuery()
            ->getArrayResult()
            ;
    }

}