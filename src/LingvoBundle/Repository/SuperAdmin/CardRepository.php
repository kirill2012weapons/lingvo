<?php

namespace LingvoBundle\Repository\SuperAdmin;


use Doctrine\ORM\EntityRepository;
use LingvoBundle\Entity\SuperAdmin\CardInformation;

class CardRepository extends EntityRepository
{

    public function getAllCards($cardNumber = null, $buyInformation = null, $isActive = null, $isUsing = null)
    {
        $query = $this->createQueryBuilder('cards')
            ->select('cards')
        ;

        $parameters = [];

        if (!is_null($cardNumber)) {
            $query
                ->andWhere('cards.cardNumber = :cardNumber')
                ->setParameter('cardNumber', $cardNumber);

        }
        if (!is_null($buyInformation)) {
            $query
                ->andWhere('cards.buyInformation LIKE :buyInformation')
                ->setParameter('buyInformation', '%' .$buyInformation. '%');
        }
        if (!is_null($isActive)) {
            $query
                ->andWhere('cards.isActive = :isActive')
                ->setParameter('isActive', $isActive ? '1' : '0');
        }

        if (!is_null($isUsing)) {
            $query
                ->andWhere('cards.isUsing = :isUsing')
                ->setParameter('isUsing', $isUsing ? '1' : '0');
        }
        return $query
            ->orderBy('cards.created_at', 'DESC')
            ->getQuery()
            ->getArrayResult()
        ;
    }

}