<?php

namespace LingvoBundle\Service\Node;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Security;

class NodeFactory
{

    public static function createNode(TokenStorage $security)
    {

        $node = new Node();
        $node->setTitle('Default Title For Node Fuck U');
        $node->setDescription('Default Description KEK');
        $node->setUser(
            $security->getToken()->getUser()
        );

        return $node;

    }
}