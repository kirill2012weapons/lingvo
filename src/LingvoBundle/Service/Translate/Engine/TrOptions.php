<?php

namespace LingvoBundle\Service\Translate\Engine;


use LingvoBundle\Service\Translate\Engine\Interfaces\ITrOptions;

class TrOptions implements ITrOptions
{

    private $url                = null;
    private $xRapidApiHost      = null;
    private $xRapidApiKey       = null;
    private $languages       = [];

    final public function __construct(
        $url,
        $xRapidApiHost,
        $xRapidApiKey,
        $languages
    ) {
        $this->url              = $url;
        $this->xRapidApiHost    = $xRapidApiHost;
        $this->xRapidApiKey     = $xRapidApiKey;
        $this->languages        = $languages;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function getAPIHost()
    {
        return $this->xRapidApiHost;
    }

    public function getAPIKey()
    {
        return $this->xRapidApiKey;
    }

    public function getLangArray()
    {
        return $this->languages;
    }
}