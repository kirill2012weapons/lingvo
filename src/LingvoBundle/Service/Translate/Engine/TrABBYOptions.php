<?php

namespace LingvoBundle\Service\Translate\Engine;


use LingvoBundle\Service\Translate\Engine\Interfaces\ITrOptions;

class TrABBYOptions implements ITrOptions
{

    private $url                = null;
    private $host               = null;
    private $apiKey             = null;
    private $languages       = [];

    final public function __construct(
        $url,
        $host,
        $apiKey,
        $languages
    ) {
        $this->url              = $url;
        $this->host             = $host;
        $this->apiKey           = $apiKey;
        $this->languages        = $languages;
    }

    public function getURL()
    {
        return $this->url;
    }

    public function getAPIHost()
    {
        return $this->host;
    }

    public function getAPIKey()
    {
        return $this->apiKey;
    }

    public function getLangArray()
    {
        return $this->languages;
    }
}