<?php

namespace LingvoBundle\Service\Translate\Engine\Parser;


use LingvoBundle\Service\Translate\Engine\Parser\Interfaces\CheckerEngine;
use LingvoBundle\Service\Translate\Engine\Parser\Interfaces\IParser;

class CustomChecker extends CheckerEngine implements IParser
{

    public function checkTheInputs($word, $source, $target)
    {
        return parent::checkTheInputs($word, $source, $target);
    }

}