<?php

namespace LingvoBundle\Service\Translate\Engine\Parser\Interfaces;


use LingvoBundle\Service\Translate\Engine\Parser\AbsEngineParser;

class CheckerEngine extends AbsEngineParser implements IParserChecker
{
    public function checkTheInputs($word, $source, $target)
    {

        $this->setWord($word);
        $this->setSourceName($source);
        $this->setTargetName($target);

        if (is_null($this->getWord())               ||
            is_null($this->getSourceName())         ||
            is_null($this->getTargetName()))        $this->setError('Incorrect inputs, tell this shit for administrator!');

        $class_name = get_class($this->getLanguageAbby());

        if (empty($class_name)) $this->setError('The class ABBY is not defined!');

        try {
            $constantReflexSource = new \ReflectionClassConstant($class_name, $this->getSourceName());
            $constantReflexTarget = new \ReflectionClassConstant($class_name, $this->getTargetName());
            $this->setSourceCode($constantReflexSource->getValue());
            $this->setTargetCode($constantReflexTarget->getValue());
        } catch (\ReflectionException $e) {
            $this->setError('No languages supported!');
        }

        return true;

    }

}