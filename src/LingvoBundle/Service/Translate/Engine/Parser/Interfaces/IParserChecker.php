<?php

namespace LingvoBundle\Service\Translate\Engine\Parser\Interfaces;


interface IParserChecker
{

    public function checkTheInputs($word, $source, $target);       // Check the inputs is exists

}