<?php

namespace LingvoBundle\Service\Translate\Engine\Parser\Interfaces;


interface IParser
{
    public function getSourceCode();        // Get the source lang code

    public function getTargetCode();        // Get the target lang code - example - 1200, 1690

    public function getSourceName();        // Get the source lang - example - EN, RU

    public function getTargetName();        // Get the target lang - example - EN, RU

    public function getWord();              // Get the word to be translated

}