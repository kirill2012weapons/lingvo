<?php

namespace LingvoBundle\Service\Translate\Engine\Parser;


use AbbyyLingvo\Languages;
use LingvoBundle\Service\Translate\Engine\Parser\Interfaces\IParser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;

abstract class AbsEngineParser implements IParser
{
    /**
     * @var integer
     */
    private $sourceCode = null;
    /**
     * @var integer
     */
    private $targetCode = null;
    /**
     * @var string
     */
    private $sourceName = null;
    /**
     * @var string
     */
    private $targetName = null;

    /**
     * @var string
     * The Word That must be translated
     */
    private $word = null;

    /**
     * @var FlashBag
     */
    private $flash              =       null;

    /**
     * @var Languages|null
     */
    private $languageAbby       =       null;

    /**
     * @var RouterInterface
     */
    private $router;

    final public function __construct($session, $languageAbby, $router)
    {
        /**
         * @var $session Session
         */

        $this->flash            = $session->getFlashBag();
        $this->languageAbby     = $languageAbby;
        $this->router            = $router;
    }

    /**
     * @return mixed
     */
    public function getSourceCode()
    {
        return $this->sourceCode;
    }

    /**
     * @param mixed $sourceCode
     */
    public function setSourceCode($sourceCode)
    {
        $this->sourceCode = $sourceCode;
    }

    /**
     * @return mixed
     */
    public function getTargetCode()
    {
        return $this->targetCode;
    }

    /**
     * @param mixed $targetCode
     */
    public function setTargetCode($targetCode)
    {
        $this->targetCode = $targetCode;
    }

    /**
     * @return mixed
     */
    public function getSourceName()
    {
        return $this->sourceName;
    }

    /**
     * @param mixed $sourceName
     */
    public function setSourceName($sourceName)
    {
        $this->sourceName = $sourceName;
    }

    /**
     * @return mixed
     */
    public function getTargetName()
    {
        return $this->targetName;
    }

    /**
     * @param mixed $targetName
     */
    public function setTargetName($targetName)
    {
        $this->targetName = $targetName;
    }

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param mixed $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }

    /**
     * @return FlashBag
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * @return Languages|null
     */
    public function getLanguageAbby()
    {
        return $this->languageAbby;
    }

    /**
     * @return RouterInterface
     */
    public function getRouter()
    {
        return $this->router;
    }

    public function setError($error) {

        $this->getFlash()
            ->set('error', $error);

        return new RedirectResponse(
            $this->getRouter()
            ->generate('index')
        );

    }

}