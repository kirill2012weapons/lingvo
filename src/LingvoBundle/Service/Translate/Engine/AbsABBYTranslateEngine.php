<?php

namespace LingvoBundle\Service\Translate\Engine;


use AbbyyLingvo\Api;
use AbbyyLingvo\Languages;
use LingvoBundle\Service\Translate\Engine\Interfaces\ITranslate;
use LingvoBundle\Service\Translate\Engine\Interfaces\ITrOptions;
use LingvoBundle\Service\Translate\Engine\Parser\CustomChecker;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class AbsABBYTranslateEngine implements ITranslate
{

    /**
     * @var Api
     */
    private $api                =       null;

    /**
     * @var FlashBag
     */
    private $flash              =       null;

    /**
     * @var Languages|null
     */
    private $languageAbby       =       null;

    /**
     * @var CustomChecker
     */
    private $checker            =       null;

    public function __construct(
        Api                     $api,
        Session                 $session,
        Languages               $languageAbby,
        CustomChecker           $customChecker
    )
    {
        $this->api              = $api;
        $this->flash            = $session->getFlashBag();
        $this->languageAbby     = $languageAbby;
        $this->checker          = $customChecker;
    }

    public function translate($sourceLng, $targetLang, $input) {

        $result = [];

        $this->checker->checkTheInputs($input, $sourceLng, $targetLang);
        $constantValueSource = $this->checker->getSourceCode();
        $constantValueTarget = $this->checker->getTargetCode();

        try {

            $trList = $this->api->minicard($input, $constantValueSource, $constantValueTarget);
            $result['result'] = $trList['Translation']['Translation'];
            $result['word'] = $input;
            $result['SourceLanguage'] = $sourceLng;
            $result['TargetLanguage'] = $targetLang;

        } catch (\Exception $exception) {
            $this->flash->add('error', $exception->getMessage());
            return $result;
        }

        return $result;

    }

}