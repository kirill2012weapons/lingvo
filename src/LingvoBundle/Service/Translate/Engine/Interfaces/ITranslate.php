<?php

namespace LingvoBundle\Service\Translate\Engine\Interfaces;


interface ITranslate
{

    public function translate($sourceLng, $targetLang, $input);                //          Translate functions

}