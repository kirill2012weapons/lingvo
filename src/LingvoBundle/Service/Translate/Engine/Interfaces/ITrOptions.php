<?php

namespace LingvoBundle\Service\Translate\Engine\Interfaces;


interface ITrOptions
{

    /**
     * @return string
     */
    public function getURL();                       //          Set RAPID translations url -
                                                    //          get this URL on this site from the acc

    /**
     * @return string
     */
    public function getAPIHost();                   //          Sets in REQUEST HEADER

    /**
     * @return string
     */
    public function getAPIKey();                    //          KEY for the applications

    /**
     * @return array
     */
    public function getLangArray();                 //          get all languages as array

}