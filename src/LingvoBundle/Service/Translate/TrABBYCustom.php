<?php

namespace LingvoBundle\Service\Translate;


use LingvoBundle\Service\Translate\Engine\AbsABBYTranslateEngine;

class TrABBYCustom extends AbsABBYTranslateEngine
{

    public function translate($sourceLng, $targetLang, $input)
    {
        $translation = parent::translate($sourceLng, $targetLang, $input);

        if (isset($translation['result']) && !empty($translation['result'])) {

            $stringTr = preg_replace('/\;\s|\,\s|\;/', ',', $translation['result']);
            $translation['resultMass'] = explode(',', $stringTr);

        }

        return $translation;
    }

}