<?php

namespace LingvoBundle\Entity\Form;


use Symfony\Component\Validator\Constraints as Assert;

class AddWordToList
{

    /**
     * @Assert\NotNull()
     * @Assert\Regex(
     *     pattern="/([A-Za-zА-ЯЁа-яё\n\-0-9])/"
     * )
     */
    private $word;

    /**
     * @Assert\NotNull()
     * @Assert\Regex(
     *     pattern="/^[A-Z]{2}/"
     * )
     */
    private $sourceL;

    /**
     * @Assert\NotNull()
     * @Assert\Regex(
     *     pattern="/^[A-Z]{2}/"
     * )
     */
    private $targetL;

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param mixed $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }

    /**
     * @return mixed
     */
    public function getSourceL()
    {
        return $this->sourceL;
    }

    /**
     * @param mixed $sourceL
     */
    public function setSourceL($sourceL)
    {
        $this->sourceL = $sourceL;
    }

    /**
     * @return mixed
     */
    public function getTargetL()
    {
        return $this->targetL;
    }

    /**
     * @param mixed $targetL
     */
    public function setTargetL($targetL)
    {
        $this->targetL = $targetL;
    }

}