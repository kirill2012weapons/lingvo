<?php

namespace LingvoBundle\Entity\SuperAdmin;


use Doctrine\ORM\Mapping as ORM;
use LingvoBundle\Validator\Constraints as AcmeAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(
 *     repositoryClass="LingvoBundle\Repository\SuperAdmin\CardRepository"
 * )
 * @ORM\Table(
 *     name="card_informations",
 * )
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("cardNumber")
 */
class CardInformation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(
     *     type="bigint",
     *     length=20,
     *     unique=true
     * )
     * @Assert\NotBlank(groups={"add"})
     */
    private $cardNumber;

    /**
     * @var string
     * @ORM\Column(
     *     type="json_array"
     * )
     * @Assert\NotBlank(groups={"add", "change"})
     * @AcmeAssert\IsJson(groups={"add", "change"})
     */
    private $json;

    /**
     * @var bool
     * @ORM\Column(
     *     type="boolean"
     * )
     * @Assert\Regex(
     *     groups={"change"},
     *     pattern="/^(?i)(true|false)$/"
     * )
     */
    private $isActive;

    /**
     * @var bool
     * @ORM\Column(
     *     type="boolean"
     * )
     * @Assert\Regex(
     *     groups={"change"},
     *     pattern="/^(?i)(true|false)$/"
     * )
     */
    private $isUsing;

    /**
     * @var string
     * @ORM\Column(
     *     type="text"
     * )
     */
    private $buyInformation;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime"
     * )
     */
    private $created_at;

    /**
     * @var \DateTime
     * @ORM\Column(
     *     type="datetime"
     * )
     */
    private $modified_at;

    public function __construct()
    {
        $this->isActive = true;
        $this->isUsing = false;
        $this->buyInformation = '';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @param string $json
     */
    public function setJson($json)
    {
        $this->json = $json;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function getIsUsing()
    {
        return $this->isUsing;
    }

    /**
     * @param bool $isUsing
     */
    public function setIsUsing($isUsing)
    {
        $this->isUsing = $isUsing;
    }

    /**
     * @return string
     */
    public function getBuyInformation()
    {
        return $this->buyInformation;
    }

    /**
     * @param string $buyInformation
     */
    public function setBuyInformation($buyInformation)
    {
        $this->buyInformation = $buyInformation;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * @param \DateTime $modified_at
     */
    public function setModifiedAt($modified_at)
    {
        $this->modified_at = $modified_at;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }


}