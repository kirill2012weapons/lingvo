<?php

namespace LingvoBundle\Entity\Security;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LingvoBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Regex(
     *     pattern="/^(?!.*([\-\_])\1{1})[A-Za-z]{2}[A-Za-z0-9\_\-]{0,10}[A-Za-z0-9]{1}$/",
     *     message="Can contain '-','_','0-9','A-Za-z'. Start with symbols 'A-Za-z', end with symbol - 'A-Za-z0-9', Minimum - 3 symbols."
     * )
     */
    private $username;

    /**
     * @Assert\NotBlank
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="LingvoBundle\Entity\Translating\Russian", inversedBy="users")
     * @ORM\JoinTable(name="users_russian")
     */
    private $russians;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="LingvoBundle\Entity\Translating\English", inversedBy="users")
     * @ORM\JoinTable(name="users_english")
     */
    private $englishes;

    public function __construct()
    {
        $this->roles              = ['ROLE_USER'];
        $this->russians           = new ArrayCollection();
        $this->englishes          = new ArrayCollection();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($role)
    {
        $this->roles[] = $role;
    }


    public function eraseCredentials()
    {

    }

    /**
     * @return ArrayCollection
     */
    public function getRussian()
    {
        return $this->russians;
    }

    /**
     * @param $russian
     */
    public function setRussian($russian)
    {
        $this->russians->add($russian);
    }

    /**
     * @return ArrayCollection
     */
    public function getEnglish()
    {
        return $this->englishes;
    }

    /**
     * @param $english
     */
    public function setEnglish($english)
    {
        $this->englishes->add($english);
    }


    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }
}