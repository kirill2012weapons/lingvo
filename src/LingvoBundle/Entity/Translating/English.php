<?php

namespace LingvoBundle\Entity\Translating;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use LingvoBundle\Validator\Constraints as AcmeAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(
 *     repositoryClass="LingvoBundle\Repository\EnglishRepository"
 * )
 * @UniqueEntity(fields="word", message="The word {{ value }} is already exist.")
 * @ORM\Table(
 *     name="english_tbl",
 *     indexes={@ORM\Index(name="en_idx", columns={"word"})}
 * )
 */
class English
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="word", length=200, unique=true)
     */
    private $word;

    /**
     * @ORM\ManyToMany(targetEntity="Russian", mappedBy="englishes")
     */
    private $russians;

    /**
     * @ORM\ManyToMany(targetEntity="LingvoBundle\Entity\Security\User", mappedBy="englishes")
     */
    private $users;

    public function __construct()
    {
        $this->russians      = new ArrayCollection();
        $this->users        = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param mixed $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }

    /**
     * @return mixed
     */
    public function getRussian()
    {
        return $this->russians;
    }

    /**
     * @param mixed $russian
     */
    public function setRussian($russian)
    {
        $this->russians->add($russian);
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $user
     */
    public function setUsers($user)
    {
        $this->users->add($user);
    }

}