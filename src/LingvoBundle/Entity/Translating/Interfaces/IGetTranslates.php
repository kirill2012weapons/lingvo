<?php

namespace LingvoBundle\Entity\Translating\Interfaces;


interface IGetTranslates
{

    public function getTranslatesAsArray();

}