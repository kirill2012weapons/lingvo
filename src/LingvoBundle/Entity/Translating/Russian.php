<?php

namespace LingvoBundle\Entity\Translating;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use LingvoBundle\Validator\Constraints as AcmeAssert;

/**
 * @ORM\Entity(
 *     repositoryClass="LingvoBundle\Repository\EnglishRepository"
 * )
 * @UniqueEntity(fields="word", message="The word {{ value }} is already exist.")
 * @ORM\Table(
 *     name="russian_tbl",
 *     indexes={@ORM\Index(name="ru_idx", columns={"word"})}
 * )
 */
class Russian
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="word", length=200, unique=true)
     */
    private $word;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="English", inversedBy="russians")
     * @ORM\JoinTable(name="russian_english")
     */
    private $englishes;

    /**
     * @ORM\ManyToMany(targetEntity="LingvoBundle\Entity\Security\User", mappedBy="russians")
     */
    private $users;

    public function __construct()
    {
        $this->englishes      = new ArrayCollection();
        $this->users        = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param mixed $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }

    /**
     * @return ArrayCollection
     */
    public function getEnglish()
    {
        return $this->englishes;
    }

    /**
     * @param $english
     */
    public function setEnglish($english)
    {
        $this->englishes->add($english);
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users->add($users);
    }

}