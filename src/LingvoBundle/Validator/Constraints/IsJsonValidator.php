<?php

namespace LingvoBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class IsJsonValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof IsJson) {
            throw new UnexpectedTypeException($constraint, IsJson::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        json_decode($value);

        if (json_last_error())
            $this->context->buildViolation($constraint->message)
            ->addViolation();

    }

}