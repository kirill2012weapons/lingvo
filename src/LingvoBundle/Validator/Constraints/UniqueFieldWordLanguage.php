<?php

namespace LingvoBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueFieldWordLanguage extends Constraint
{

    public $message = 'The word \""{{ value }}"\" is already exists in database. Change ur dictionary or add new translate.';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }

}