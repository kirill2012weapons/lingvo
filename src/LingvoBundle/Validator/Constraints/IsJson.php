<?php

namespace LingvoBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsJson extends Constraint
{

    public $message = 'It must Be JSON STRING!';

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }

}