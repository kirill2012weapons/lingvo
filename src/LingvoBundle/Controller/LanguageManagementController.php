<?php

namespace LingvoBundle\Controller;


use Doctrine\ORM\EntityManager;
use LingvoBundle\Entity\Form\AddWordToList;
use LingvoBundle\Entity\Security\User;
use LingvoBundle\Entity\Translating\English;
use LingvoBundle\Entity\Translating\Russian;
use LingvoBundle\Form\LanguageManagement\AddWordToListType;
use LingvoBundle\Service\Node\Node;
use LingvoBundle\Service\Translate\TrABBYCustom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\TraceableValidator;

class LanguageManagementController extends Controller
{
    public function addWordToList(Request $request)
    {
        /**
         * @var $em                             EntityManager
         * @var $addWordToListEntity            AddWordToList
         * @var $form                           Form
         * @var $errors                         FormErrorIterator
         * @var $abbyTr                         TrABBYCustom
         * @var $user                           User
         * @var $validator                      TraceableValidator
         * @var $node                           Node
         */

        $node = $this->get('node');

        $addWordToListEntity = new AddWordToList();
        $form = $this->createForm(AddWordToListType::class, $addWordToListEntity);

        $form->handleRequest($request);

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {

            $validator = $this->get('validator');
            $errorsConstraintViolationList = [];

            $em = $this->getDoctrine()->getManager();
            $abbyTr = $this->get('translation.abby.main');

            $result = $abbyTr->translate(
                $addWordToListEntity->getSourceL(),
                $addWordToListEntity->getTargetL(),
                $addWordToListEntity->getWord()
            );

            if ($addWordToListEntity->getSourceL() == 'EN') {

                $engEntity = new English();
                $engEntity->setWord($addWordToListEntity->getWord());
                $engEntity->setUsers($user);

                $errorsConstraintViolationList[] = $validator->validate($engEntity);

                $user->setEnglish($engEntity);

                foreach ($result['resultMass'] as $ru) {
                    $ruEntity = new Russian();
                    $ruEntity->setWord($ru);
                    $ruEntity->setUsers($user);
                    $ruEntity->setEnglish($engEntity);
                    $errorsConstraintViolationList[] = $validator->validate($ruEntity);
                    $em->persist($ruEntity);
                    $engEntity->setRussian($ruEntity);
                    $user->setRussian($ruEntity);
                }
                $em->persist($user);
                $em->persist($engEntity);

            } else {

                $ruEntity = new Russian();
                $ruEntity->setWord($addWordToListEntity->getWord());
                $ruEntity->setUsers($user);

                $errorsConstraintViolationList[] = $validator->validate($ruEntity);

                $user->setRussian($ruEntity);

                foreach ($result['resultMass'] as $eng) {
                    $engEntity = new English();
                    $engEntity->setWord($eng);
                    $engEntity->setUsers($user);
                    $engEntity->setRussian($ruEntity);
                    $errorsConstraintViolationList[] = $validator->validate($engEntity);
                    $em->persist($engEntity);
                    $ruEntity->setEnglish($engEntity);
                    $user->setEnglish($engEntity);
                }
                $em->persist($user);
                $em->persist($ruEntity);

            }

            try {

                $broke = false;
                foreach ($errorsConstraintViolationList as $violation) {
                    /**
                     * @var $violation ConstraintViolationList
                     */
                    foreach ($violation as $item) {
                        $this->addFlash('error', $item->getMessage());
                        $broke = true;
                    }
                }

                if (!$broke) {
                    $this->addFlash('success', 'Add Word to urs vocabulary');
                    $em->flush();
                }

            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->addFlash('error', $exception->getMessage());
            }

        } else {
            $this->addFlash('error', 'Something is broke');
            $errors = $form->getErrors();
            foreach ($errors as $error) {
                $this->addFlash('error', $error->getMessage());
            }
        }

        return $this->redirectToRoute('index');

//        return $this->render('@lingvo/Controllers/Test/index.html.twig');

    }
}
