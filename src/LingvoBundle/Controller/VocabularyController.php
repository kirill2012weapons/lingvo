<?php

namespace LingvoBundle\Controller;


use Doctrine\ORM\EntityManager;
use LingvoBundle\Entity\Translating\English;
use LingvoBundle\Repository\EnglishRepository;
use LingvoBundle\Service\Node\Node;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VocabularyController extends Controller
{
    public function vocabulary(Request $request, $username)
    {

        /**
         * @var $node           Node
         * @var $em             EntityManager
         * @var $engRep         EnglishRepository
         */

        $node = $this->get('node');
        $node->setTitle('Ur Vocabulary FUCKER!');

        $em = $this->getDoctrine()->getManager();
        $engRep = $em->getRepository(English::class);

        $results = $engRep->getTranslatesAsArray();

        return $this->render('@lingvo/Controllers/VocabularyController/index.html.twig', [
            'node'          => $node,
            'results'        => $results,
        ]);
    }
}
