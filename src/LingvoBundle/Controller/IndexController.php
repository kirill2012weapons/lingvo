<?php

namespace LingvoBundle\Controller;


use AbbyyLingvo\Api;
use AbbyyLingvo\Languages;
use LinguaLeo\wti\WtiApi;
use LingvoBundle\Entity\Form\AddWordToList;
use LingvoBundle\Entity\Form\SearchIndex;
use LingvoBundle\Form\LanguageManagement\AddWordToListType;
use LingvoBundle\Form\Search\SearchIndexType;
use LingvoBundle\Service\Node\Node;
use LingvoBundle\Service\Translate\TrABBYCustom;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {

        /**
         * @var $search                     SearchIndex
         * @var $formSearch                 SearchIndexType
         * @var $trAbby                     TrABBYCustom
         * @var $formAddWordToList          CardType
         * @var $node                       Node
        */

        $node = $this->get('node');

        $search = new SearchIndex();

        $formSearch = $this->createForm(SearchIndexType::class, $search);
        $formSearch->handleRequest($request);

        $formAddWordToList = $this->createForm(AddWordToListType::class, new AddWordToList());

        $result = [];


        if ($formSearch->isSubmitted() && $formSearch->isValid()) {

            $trAbby = $this->get('translation.abby.main');

            $result = $trAbby->translate(
                $search->getSource(),
                $search->getTarget(),
                $search->getTranslate()
            );

        }

        return $this->render('@lingvo/Controllers/Index/index.html.twig', [
            'node'                  => $node,
            'formSearch'            => $formSearch->createView(),
            'result'                => $result,
            'formAddWordToList'    => $formAddWordToList->createView(),
        ]);
    }
}
