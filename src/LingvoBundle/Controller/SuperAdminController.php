<?php

namespace LingvoBundle\Controller;


use Doctrine\ORM\EntityManager;
use LingvoBundle\Entity\SuperAdmin\CardInformation;
use LingvoBundle\Form\SuperAdmin\CardType;
use LingvoBundle\Form\SuperAdmin\SearchCardType;
use LingvoBundle\Repository\SuperAdmin\CardRepository;
use LingvoBundle\Service\Node\Node;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\TraceableValidator;

class SuperAdminController extends Controller
{
    public function addCard(Request $request)
    {
        /**
         * @var $node                   Node
         * @var $cardInformation        CardInformation
         * @var $em                     EntityManager
         * @var $validator              TraceableValidator
         */
        $node = $this->get('node');

        $validator = $this->get('validator');

        $cardInformation = new CardInformation();

        $form = $this->createForm(CardType::class, $cardInformation, [
            'validation_groups' => ['add'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && ($validator->validate($cardInformation)->count() == 0)) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($cardInformation);

            try {
                $em->flush();
                $this->addFlash('success', 'New card successfully added');
                return $this->redirectToRoute('suprime_card_list');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->redirectToRoute('suprime_card_list');
            }

        }
        if ($validator->validate($cardInformation)->count() != 0) $this->addFlash('error', 'Card is exist');

        return $this->render('@lingvo/Controllers/SuperAdmin/addCard.html.twig', [
            'node'          => $node,
            'form'          => $form->createView(),
        ]);
    }

    public function cardList(Request $request)
    {

        /**
         * @var $node                   Node
         * @var $em                     EntityManager
         * @var $cardRepository         CardRepository
         * @var $searchForm             SearchCardType
         */

        $node = $this->get('node');
        $em = $this->getDoctrine()->getManager();
        $cardRepository = $em->getRepository(CardInformation::class);

        $searchForm = $this->createForm(SearchCardType::class);
        $searchForm->handleRequest($request);

        return $this->render('@lingvo/Controllers/SuperAdmin/index.html.twig', [
            'node'          => $node,
            'result'        => $cardRepository->getAllCards(
                $searchForm->get('searchCardNumber')->getData(),
                $searchForm->get('buyInformation')->getData(),
                $searchForm->get('isActive')->getData(),
                $searchForm->get('isUsing')->getData()
            ),
            'searchForm'    => $searchForm->createView(),
        ]);

    }

    public function cardChange($id, Request $request)
    {

        /**
         * @var $node                   Node
         * @var $em                     EntityManager
         * @var $cardRepository         CardRepository
         * @var $validator              TraceableValidator
         */

        $node = $this->get('node');
        $em = $this->getDoctrine()->getManager();
        $cardRepository = $em->getRepository(CardInformation::class);

        $validator = $this->get('validator');

        $card = $cardRepository->find($id);

        $form = $this->createForm(CardType::class, $card, [
            'validation_groups' => ['change'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && ($validator->validate($card)->count() == 0)) {
            try {
                $em->flush();
                $this->addFlash('success', 'The card had been changed.');
                return $this->redirectToRoute('suprime_card_list');
            } catch (\Doctrine\ORM\OptimisticLockException $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->redirectToRoute('suprime_card_list');
            }
        }

        if ($validator->validate($card)->count() != 0) $this->addFlash('error', 'Card is exist');

        return $this->render('@lingvo/Controllers/SuperAdmin/change.html.twig', [
            'node'          => $node,
            'form'          => $form->createView(),
        ]);

    }

    public function cardDelete($id, Request $request)
    {

        /**
         * @var $node                   Node
         * @var $em                     EntityManager
         * @var $cardRepository         CardRepository
         */

        $node = $this->get('node');
        $em = $this->getDoctrine()->getManager();
        $cardRepository = $em->getRepository(CardInformation::class);

        $card = $cardRepository->find($id);

        try {
            $em->remove($card);
            $em->flush();
            $this->addFlash('success', 'The card had been changed.');
            return $this->redirectToRoute('suprime_card_list');
        } catch (\Doctrine\ORM\OptimisticLockException $exception) {
            $this->addFlash('error', $exception->getMessage());
            return $this->redirectToRoute('suprime_card_list');
        }

    }

}
