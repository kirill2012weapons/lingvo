<?php

namespace LingvoBundle\Controller;


use LingvoBundle\Entity\Security\User;
use LingvoBundle\Form\Security\UserCheckInType;
use LingvoBundle\Form\Security\UserLoginType;
use LingvoBundle\Service\Node\Node;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends Controller
{

    public function login()
    {

        /**
         * @var $node           Node
         */

        $node = $this->get('node');
        $node->setTitle('LOGIN fuckers');

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();


        $form = $this->createForm(UserLoginType::class, [
            '_username' => $lastUsername,
        ]);

        return $this->render('@lingvo/Controllers/Security/login.html.twig', [
            'node'          => $node,
            'form'          => $form->createView(),
            'error'         => $error,
        ]);
    }

    public function logout()
    {
        return $this->redirectToRoute('index');
    }

    public function checkIn(
        Request $request,
        UserPasswordEncoderInterface $encoder
    )
    {

        /**
         * @var $node           Node
         */

        $node = $this->get('node');
        $node->setTitle('Checkin FUCKERS!');

        $user = new User();
        $form = $this->createForm(UserCheckInType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('@lingvo/Controllers/Security/check_in.html.twig', [
            'node'          =>          $node,
            'form'          =>          $form->createView(),
        ]);
    }

}