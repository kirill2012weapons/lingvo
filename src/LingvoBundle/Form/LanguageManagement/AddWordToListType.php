<?php

namespace LingvoBundle\Form\LanguageManagement;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddWordToListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('word', TextType::class, [
                'label' => 'Word',
                'required' => true,
            ])
            ->add('sourceL', TextType::class, [
                'label' => 'sourceL',
                'required' => true,
            ])
            ->add('targetL', TextType::class, [
                'label' => 'targetL',
                'required' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => \LingvoBundle\Entity\Form\AddWordToList::class,
        ]);
    }

}