<?php

namespace LingvoBundle\Form\SuperAdmin;


use LingvoBundle\Entity\SuperAdmin\CardInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;

class SearchCardType extends AbstractType
{

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->setAction(
                $this->router->generate('suprime_card_list')
            )
            ->add('searchCardNumber', IntegerType::class, [
                'label'     => 'Search Value By Card Number',
                'required' => false,
            ])
            ->add('buyInformation', TextareaType::class, [
                'label' => 'Search buy info like',
                'required' => false,
                'attr' => [
                    'rows' => 2,
                ]
            ])
            ->add('isActive', ChoiceType::class, [
                'choices' => array(
                    'Active' => 'true',
                    'Not Active' => 'false',
                ),
                'expanded' => true,
                'required' => false,
            ])
            ->add('isUsing', ChoiceType::class, [
                'choices' => array(
                    'Using' => 'true',
                    'Not Using' => 'false',
                ),
                'expanded' => true,
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label'     => 'SEARCH',
                'attr'      => [
                    'style' => 'width: 100px;'
                ],
            ])
            ->add('reset', SubmitType::class, [
                'label'     => 'RESET',
                'attr'      => [
                    'class' => 'btn-outline-info',
                    'style' => 'width: 100px;'
                ],
            ])
        ;

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            /**
             * @var $subBtn     SubmitButton
             * @var $form       Form
             */
            $subBtn = $form->getClickedButton();
            if ($subBtn->getName() == 'reset') {
                $r = new RedirectResponse($this->router->generate('suprime_card_list'));
                $r->send();
            }
        });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }


}