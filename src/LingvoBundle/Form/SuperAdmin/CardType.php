<?php

namespace LingvoBundle\Form\SuperAdmin;


use Doctrine\ORM\EntityRepository;
use LingvoBundle\Entity\SuperAdmin\CardInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardNumber', IntegerType::class, [
                'label' => 'Card Number (Uniq)'
            ])
            ->add('json', TextareaType::class, [
                'label' => 'Put JSON INFORMATION',
                'attr' => [
                    'rows' => 9,
                ]
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /**
             * @var $data               CardInformation
             */
            $form = $event->getForm();
            $data = $event->getData();
            if (!in_array('change', $form->getConfig()->getOption('validation_groups'))) {
                $form->add('submit', SubmitType::class);
                return;
            }

            dump($data->getIsActive());

            $form
                ->add('isActive', ChoiceType::class, [
                    'choices' => array(
                        'Active' => 'true',
                        'Not Active' => 'false',
                    ),
                    'expanded' => true,
                    'data' => $data->getIsActive() ? 'true' : 'false',
                ])
                ->add('isUsing', ChoiceType::class, [
                    'choices' => array(
                        'Using' => 'true',
                        'Not Using' => 'false',
                    ),
                    'expanded' => true,
                    'data' => $data->getIsUsing() ? 'true' : 'false',
                ])
                ->add('buyInformation', TextareaType::class, [
                    'label' => 'Card Additional Information.',
                    'required' => false,
                    'attr' => [
                        'rows' => 6,
                    ]
                ])
                ->add('submit', SubmitType::class);
            ;

        });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardInformation::class,
        ]);
    }

}