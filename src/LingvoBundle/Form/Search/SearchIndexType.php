<?php

namespace LingvoBundle\Form\Search;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SearchIndexType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('translate', TextType::class, [
                'label' => 'Translate',
                'attr' => ['class' => 'form-group'],
            ])
            ->add('source', ChoiceType::class, [
                'label' => 'Source language',
                'attr' => ['class' => 'custom-control custom-radio form-group'],
                'choices' => [
                    'English' => 'EN',
                    'Russian' => 'RU',
                ],
                'expanded' => true,
                'choices_as_values' => true,
                'multiple'=>false,
                'data' => 'EN',
            ])
            ->add('target', ChoiceType::class, [
                'label' => 'Target language',
                'attr' => ['class' => 'custom-control custom-radio form-group'],
                'choices' => [
                    'English' => 'EN',
                    'Russian' => 'RU',
                ],
                'expanded' => true,
                'choices_as_values' => true,
                'multiple'=>false,
                'data' => 'RU',
            ])
        ;
    }


}