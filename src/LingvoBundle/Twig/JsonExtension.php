<?php

namespace LingvoBundle\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class JsonExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('jsonDecode', [$this, 'jsonDecode']),
        ];
    }

    public function jsonDecode($string)
    {
        $obj = json_decode($string);
        $outPut = '<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">KEY</th>
      <th scope="col">Value</th>
    </tr>
  </thead>
  <tbody>';

        foreach ($obj as $key => $value) {
            $outPut .= '
                <tr style="font-size: 12px;">
                  <th scope="row">' . $key . '</th>
                  <td>' . $value . '</td>
                </tr>
    ';
        }

        $outPut .= '  </tbody>
</table> ';

        return $outPut;
    }
}