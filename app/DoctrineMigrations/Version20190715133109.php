<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190715133109 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users_russian (user_id INT NOT NULL, russian_id INT NOT NULL, INDEX IDX_1B6685BFA76ED395 (user_id), INDEX IDX_1B6685BF9D0D7BF1 (russian_id), PRIMARY KEY(user_id, russian_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_english (user_id INT NOT NULL, english_id INT NOT NULL, INDEX IDX_DC8376CAA76ED395 (user_id), INDEX IDX_DC8376CA20C67504 (english_id), PRIMARY KEY(user_id, english_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE english_tbl (id INT AUTO_INCREMENT NOT NULL, word VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_D3EF5737C3F17511 (word), INDEX en_idx (word), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE russian_tbl (id INT AUTO_INCREMENT NOT NULL, word VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_1E859AAAC3F17511 (word), INDEX ru_idx (word), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE russian_english (russian_id INT NOT NULL, english_id INT NOT NULL, INDEX IDX_FFDB576D9D0D7BF1 (russian_id), INDEX IDX_FFDB576D20C67504 (english_id), PRIMARY KEY(russian_id, english_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_russian ADD CONSTRAINT FK_1B6685BFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_russian ADD CONSTRAINT FK_1B6685BF9D0D7BF1 FOREIGN KEY (russian_id) REFERENCES russian_tbl (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_english ADD CONSTRAINT FK_DC8376CAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_english ADD CONSTRAINT FK_DC8376CA20C67504 FOREIGN KEY (english_id) REFERENCES english_tbl (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE russian_english ADD CONSTRAINT FK_FFDB576D9D0D7BF1 FOREIGN KEY (russian_id) REFERENCES russian_tbl (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE russian_english ADD CONSTRAINT FK_FFDB576D20C67504 FOREIGN KEY (english_id) REFERENCES english_tbl (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_english DROP FOREIGN KEY FK_DC8376CA20C67504');
        $this->addSql('ALTER TABLE russian_english DROP FOREIGN KEY FK_FFDB576D20C67504');
        $this->addSql('ALTER TABLE users_russian DROP FOREIGN KEY FK_1B6685BF9D0D7BF1');
        $this->addSql('ALTER TABLE russian_english DROP FOREIGN KEY FK_FFDB576D9D0D7BF1');
        $this->addSql('DROP TABLE users_russian');
        $this->addSql('DROP TABLE users_english');
        $this->addSql('DROP TABLE english_tbl');
        $this->addSql('DROP TABLE russian_tbl');
        $this->addSql('DROP TABLE russian_english');
    }
}
